На машине с установленным docker и docker-compose зайти в каталог с проектом и выполнить команду:
sudo docker-compose up -d database

После запуска mssql зайти в SSMS и создать бд с именем Hangfire.
В файле конфигурации appsettings.json в строке подключения указать имя сервера(ip-адрес или dns-имя, где запущен контейнер).
Затем выполнить команду:
sudo docker-compose up -d hostedservice