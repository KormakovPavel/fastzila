﻿
namespace Fastzila
{
    public class Setting
    {   
        public string ConnectionHangfire { get; set; }
        public string PathLog { get; set; }       
        public string CronCollectDailyStatistics { get; set; }
        public string CronNotifySubscribers { get; set; }
    }
}
