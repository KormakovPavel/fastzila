﻿using System.Threading.Tasks;

namespace Fastzila
{
    public interface IWorkerService
    {
        Task CollectDailyStatistics();
        Task NotifySubscribers();
    }
}
