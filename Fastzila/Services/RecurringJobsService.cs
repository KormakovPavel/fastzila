﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Hangfire;
using Hangfire.Server;

namespace Fastzila
{
    internal class RecurringJobsService : BackgroundService
    {
        private readonly Setting _setting;
        private readonly IRecurringJobManager _recurringJobs;
        private readonly ILogger<RecurringJobScheduler> _logger;                
        private readonly IWorkerService _workerService;
        public RecurringJobsService(IOptions<Setting> options, IRecurringJobManager recurringJobs, 
                                    ILogger<RecurringJobScheduler> logger, IWorkerService workerService)
        {
            _setting = options.Value;
            _recurringJobs = recurringJobs;
            _logger = logger;                        
            _workerService = workerService;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                _recurringJobs.AddOrUpdate("CollectDailyStatistics", () => _workerService.CollectDailyStatistics(), _setting.CronCollectDailyStatistics);
                _recurringJobs.AddOrUpdate("NotifySubscribers", () => _workerService.NotifySubscribers(), _setting.CronNotifySubscribers);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Task.CompletedTask;
        }
    }
}
