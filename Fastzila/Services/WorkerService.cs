﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace Fastzila
{
    public class WorkerService : IWorkerService
    {       
        private readonly string _fullPathSuccessLog;
        private readonly string _fullPathErrorLog;        
        public WorkerService(IOptions<Setting> options)
        {
            var setting = options.Value;

            if (!Directory.Exists(setting.PathLog))
            {
                Directory.CreateDirectory(setting.PathLog);
            }

            _fullPathSuccessLog = Path.Combine(setting.PathLog, "ws_success.log");
            _fullPathErrorLog = Path.Combine(setting.PathLog, "ws_error.log");
        }

        public async Task CollectDailyStatistics()
        {            
            try
            {
                Thread.Sleep(5000);
                var content = new List<string>() { $"{DateTime.Now.ToString("dd.MM.yyyy hh:mm:ss")}\tSuccess CollectDailyStatistics!" };

                await File.AppendAllLinesAsync(_fullPathSuccessLog, content);                
            }
            catch (Exception ex)
            {
                var content = new List<string>() { $"{DateTime.Now.ToString("dd.MM.yyyy hh:mm:ss")}\tError CollectDailyStatistics!{ex.Message}" };

                await File.AppendAllLinesAsync(_fullPathErrorLog, content);
            }
        }

        public async Task NotifySubscribers()
        {
            try
            {
                Thread.Sleep(5000);
                var content = new List<string>() { $"{DateTime.Now.ToString("dd.MM.yyyy hh:mm:ss")}\tSuccess NotifySubscribers!" };

                await File.AppendAllLinesAsync(_fullPathSuccessLog, content);
            }
            catch (Exception ex)
            {
                var content = new List<string>() { $"{DateTime.Now.ToString("dd.MM.yyyy hh:mm:ss")}\tError NotifySubscribers!{ex.Message}" };

                await File.AppendAllLinesAsync(_fullPathErrorLog, content);
            }
        }
    }
}
