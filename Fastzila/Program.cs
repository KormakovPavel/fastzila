using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Fastzila
{
    public class Program
    {
        public static void Main()
        {
            Startup.CreateHostBuilder().Build().Run();
        }
    }
}
